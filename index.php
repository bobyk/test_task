<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('PUBLIC_PATH', dirname(__FILE__));
define('APPLICATION_PATH', PUBLIC_PATH . '/app');

// For using twig from composer
require_once PUBLIC_PATH . '/vendor/autoload.php';

$aConfig = require APPLICATION_PATH . '/config.php';
require_once APPLICATION_PATH . '/libs/App.php';

\App::getInstance()
    ->setConfig($aConfig)
    ->run();
