<?php

return array(
	// Host name
	'host' => 'students.loc',

	// Database config
    'db' => array(
	    'driver' => 'mysql',
	    'host' => 'localhost',
        'db' => 'students',
        'user' => 'dev',
        'password' => '6tmwmyhZzw5E6ZR9',
        'charset' => 'utf8',
    ),

	// Routes
    'routes' => array(
	    '/' => 'index/index',
	    '/add/?' => 'student/add',
		'/edit/<id:\d+>/?' => 'student/add',
		'/delete/<id:\d+>/?' => 'student/delete',
		'/student/<id:\d+>/?' => 'student/view',
		'/students/?' => 'student/list',
	),


	// Default settings
	'templatePath' => '/layouts',
	'defaultTemplate' => 'main',
	'viewPath' => '/view',


//	'viewClass' => '\Libs\View', // My view
	'viewClass' => '\Libs\TwigView', // Twig view


	// Count results on page
	'pageSize' => 10,

);
