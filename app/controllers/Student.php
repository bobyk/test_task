<?php

namespace Controller;

use Libs\Controller;
use Libs\HttpException;
use Libs\ValidationException;
use Model\Student as StudentModel;

/**
 * CRUD for student table
 *
 * Class Student
 * @package Controller
 */
class Student extends Controller
{
	/**
	 * List students
	 *
	 * @return mixed
	 */
	public function actionList()
	{
		$oStudent = new StudentModel();

		$this->rowset = $oStudent->getList($this->_oPager);

		$this->render('student/list');
	}

	/**
	 * View full student info
	 */
	public function actionView()
	{
		$oStudent = new StudentModel();

		$this->row = $oStudent->getRow($this->_getParam('id'));

		if(empty($this->row))
			throw new HttpException(404, "Page not found");

		$this->aSex = StudentModel::getSex();

		return $this->render('student/view');
	}

	/**
	 * Add or update student.
	 *
	 * If id is empty, add new student,
	 * if id is not empty, edit student by id
	 */
	public function actionAdd()
	{
		$oStudent = new StudentModel();

		$row = $oStudent->getRow($this->_getParam('id'));

		// Save data, if request is post.
		if($this->_isPost())
		{
			$row = $this->_getPost();

			// If selected photo
			if(!empty($_FILES['photo']) && !empty($_FILES['photo']['tmp_name']))
			{
				$imgPath = '/img/-/';

				// Create directory
				mkdir(PUBLIC_PATH . $imgPath, 0755, true);

				$imgPath = $imgPath . $_FILES['photo']['name'];

				// Copy file to created directory
				copy($_FILES['photo']['tmp_name'], PUBLIC_PATH . $imgPath);

				// Set photo field in row
				$row['photo'] = $imgPath;
			}

			try {
				$itemId = $oStudent->save($row);

				// If success saved, redirect to view full student info
				if($itemId > 0)
					return $this->_redirect('/students');
			}
			// If exist exception, set messages to view
			catch(ValidationException $e) {
				$this->errors = $e->getMessages();
			}
		}

		$this->aSex = StudentModel::getSex();
		$this->row = $row;

		return $this->render('student/add');
	}

	/**
	 * Delete student.
	 *
	 * If id is empty, add new student,
	 * if id is not empty, edit student by id
	 */
	public function actionDelete()
	{
		$oStudent = new StudentModel();

		$oStudent->deleteById($this->_getParam('id'));

		$redirectUrl = '/students';

		if(!empty($_SERVER['HTTP_REFERER']))
			$redirectUrl = $_SERVER['HTTP_REFERER'];

		return $this->_redirect($redirectUrl);
	}


}