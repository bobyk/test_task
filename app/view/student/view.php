<div id="student-view">
	<a href="/students">&larr; Go back to list</a> or <a href="/edit/<?php echo $row['id']; ?>">Edit student</a>

	<h1><?php echo $row['name'] . ' '. $row['last_name']; ?></h1>

	<div class="row">
		<div class="col-md-5">
			<img alt="<?php echo $row['name'] . ' '. $row['last_name']; ?>" src="<?php echo $row['photo']; ?>" />
		</div>

		<div class="col-md-7 info">
			<p><span>Sex:</span><span><?php echo $aSex[$row['sex']]; ?></span></p>
			<p><span>Group:</span><span><?php echo $row['group']; ?></span></p>
			<p><span>Faculty:</span><span><?php echo $row['faculty']; ?></span></p>
			<p><span>Phone:</span><span><?php echo $row['phone']; ?></span></p>
			<p><span>Email:</span><span><?php echo $row['email']; ?></span></p>
		</div>
	</div>

</div>
