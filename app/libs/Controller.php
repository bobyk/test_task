<?php

namespace Libs;

/**
 * Abstract controller
 *
 * Class Controller
 * @package Libs
 */
abstract class Controller
{
	/**
	 * MVC View
	 *
	 * @var \Libs\View
	 */
	protected $_oView = null;

	/**
	 * @var \Libs\Pager
	 */
	protected $_oPager;

	/**
	 * Cunstruct object and call init method
	 */
	public function __construct()
	{
		$this->init();
	}

	/**
	 * Called before rendering page, but after action call render
	 */
	protected function _preDispatch()
	{
		$this->_oView->setVar('oPager', $this->_oPager);

		$this->_oView->setVar('action', \App::getInstance()->getAction());
		$this->_oView->setVar('controller', \App::getInstance()->getController());
	}

	/**
	 * Initialize pager
	 */
	public function init()
	{
		$page = $this->_getParam('page', 1);
		$page = abs((integer)$page);
		$page = $page > 1 ? $page : 1;
		$this->_oPager = new \Libs\Pager();
		$this->_oPager->setPage($page)
					  ->setPageSize(\App::getInstance()->getConfig()->pageSize)
					  ->setTotal(0);
	}

	/**
	 * @param $view
	 * @param null $part
	 * @param null $template
	 */
	public function render($view, $part = null, $template = null)
	{
		if($template !== null)
			$this->_oView->setTemplate($template);

		$this->_preDispatch();

		$this->_oView->setView($view, $part);
	}

	/**
	 * For short adding var to view
	 *
	 * @param $key
	 * @param $val
	 */
	public function __set($key, $val)
	{
		$this->_oView->setVar($key, $val);
	}

	/**
	 * For short getting var from view
	 *
	 * @param $key
	 * @return null
	 */
	public function __get($key)
	{
		return $this->_oView->getVar($key);
	}

	/**
	 * Short method for check isset var in view
	 *
	 * @param $key
	 * @return bool
	 */
	public function __isset($key)
	{
		return isset($this->_oView->$key);
	}

	/**
	 * Short method check var in view is empty
	 *
	 * @param $key
	 * @return bool
	 */
	public function __empty($key)
	{
		return empty($this->_oView->$key);
	}

	/**
	 * Set MVC View
	 *
	 * @param View $oView
	 */
	public function setView(\Libs\View $oView)
	{
		$this->_oView = $oView;
	}

	/**
	 * Check current request is AJAX
	 *
	 * @return bool
	 */
	protected function _isXmlHttpRequest()
	{
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}

	/**
	 * Get param value. Return $default if param is not exist
	 *
	 * @param $name
	 * @param null $default
	 * @return mixed
	 */
	protected function _getParam($name, $default = null)
	{
		if(isset($_GET[$name]))
			return $_GET[$name];

		if(isset($_POST[$name]))
			return $_POST[$name];

		return $default;
	}

	/**
	 * Get all request params
	 *
	 * @return array
	 */
	protected function _getParams()
	{
		return $_REQUEST;
	}

	/**
	 * Get all post params
	 *
	 * @return array
	 */
	protected function _getPost()
	{
		return $_POST;
	}

	/**
	 * Check current request is a post
	 *
	 * @return bool
	 */
	protected function _isPost()
	{
		return count($_POST) > 0;
	}

	/**
	 * Set header for 301 redirect
	 *
	 * @param $url
	 * @return bool
	 */
	protected function _redirect($url)
	{
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$url);

		return true;
	}
} 