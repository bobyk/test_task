<?php

namespace Libs;

/**
 * Abstract class for models
 *
 * Class Model
 * @package Libs
 */
class Model
{
	/**
	 * Table fields for checking in prepareRow()
	 *
	 * @var array
	 */
	protected $_allowFields = array();

	/**
	 * Fields for validation
	 *
	 * array (
	 * 	  'field' => array(
	 *			'filter' => FILTER_VALIDATE_*,
	 *			'regexp' => '.*',
	 *			'message' => 'Message, if data incorrect',
	 * 	  )
	 * )
	 *
	 * @var array
	 */
	protected $_fieldsValidation = array();

	/**
	 * Model instance
	 *
	 * @var self
	 */
	private static $_instance;

	/**
	 * @var \DbSimple_Connect
	 */
	private static $_driver = null;

	/**
	 * Create model instance.
	 * Using for create single database driver
	 *
	 * @return Model
	 */
	public static function getInstance()
	{
		if(self::$_instance === null)
			self::$_instance = new self;

		return self::$_instance;
	}

	/**
	 * When create object, database driver set to _db
	 */
	public function __construct()
	{
		$this->_db = self::$_driver;
	}

	/**
	 * Set database driver
	 *
	 * @param $driver
	 */
	public static function setDbDriver($driver)
	{
		self::$_driver = $driver;
	}

	/**
	 * Return static database driver
	 *
	 * @return \DbSimple_Connect
	 */
	public static function getDbDriver()
	{
		return self::$_driver;
	}

	/**
	 * Return database driver
	 *
	 * @return \DbSimple_Connect
	 */
	public function getAdapter()
	{
		return $this->_db;
	}

	/**
	 * @param $row
	 * @return array
	 */
	public function prepareRow($row)
	{
		return array_intersect_key($row, array_flip($this->_allowFields));
	}

	public function validateRow($row)
	{
		if(empty($this->_fieldsValidation))
			return false;

		$aMessages = array();

		foreach($this->_fieldsValidation as $field => $validation)
		{
			if(!isset($validation['message']))
				continue;

			if(isset($validation['filter']) && !filter_var($row[$field], $validation['filter']))
			{
				$aMessages[$field] = $validation['message'];
			}

			if(isset($validation['regexp']) && !preg_match('~^' . $validation['regexp'] . '$~i', $row[$field]))
			{
				$aMessages[$field] = $validation['message'];
			}
		}

		if(!empty($aMessages))
			throw new ValidationException($aMessages);

		return true;
	}

} 