<?php

namespace Libs;

/**
 * Handles http exception
 *
 * Class HttpException
 * @package Libs
 */
class HttpException extends \Exception
{

	/**
	 * Code is required
	 *
	 * @param int $code
	 * @param string $message
	 */
	public function __construct($code, $message)
	{
		parent::__construct($message, $code);
	}

} 